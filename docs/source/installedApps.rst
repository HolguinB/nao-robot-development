*************************
Installed Applications
*************************

SoftBank Robotics' `Application Store`_.

Below are the applications currently installed on the Nao Robot. You can click the title of the application to be taken directly to the application store page.

`Tai Chi Dance`_
#################
This applications lets Nao show off some Tai Chi moves when asked.

`Dialog Move`_
###############
Dialog Move adds some basic commands like "sit" and "lie down" to Nao. 


.. _application store: https://cloud.aldebaran-robotics.com/application/
.. _tai chi dance: https://cloud.aldebaran-robotics.com/application/taichi-dance-free/
.. _dialog move: https://cloud.aldebaran-robotics.com/application/dialog_move/
